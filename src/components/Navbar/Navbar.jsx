import React, { useState } from 'react';
import {GiHamburgerMenu} from 'react-icons/gi'
import {MdOutlineRestaurantMenu} from 'react-icons/md'
import images from '../../constants/images'

import './Navbar.css';

const Navbar = () => {

  const [toggleMenu, setToggleMenu] = useState(false);

  return (
  <nav className="app__navbar">
    <div className="app__navbar-logo">
      <img src={images.gericht} alt="gericht" />
    </div>
    <ul className="app__navbar-links">
      <li className="p__opensans">
        <a href="#home">на главную</a>
      </li>

       <li className="p__opensans">
         <a href="#about">о нас</a>
        </li>
        <li className="p__opensans">
          <a href="#menu">меню</a>
        </li> 
       
        <li className="p__opensans">
          <a href="#awards">награды</a>
        </li>
        <li className="p__opensans">
          <a href="#contacts">контакты</a>
        </li>
    </ul>
    <div className="app__navbar-login">
      <a href="#login" className="p__opensans">
          войти / зарегистрироваться
      </a>
      <div></div>
      <a href="/" className="p__opensans">
            Стол заказов
      </a>
    </div>
    <div className="app__navbar-smallscreen">
      <GiHamburgerMenu color="#fff" fontSize={27} 
      onClick={() => setToggleMenu(true)} />
      </ div>
      {toggleMenu && (
      <div className="app__navbar-smallscreen_overlay 
      flex__center slide-bottom">
        <MdOutlineRestaurantMenu 
          fontSize={27} 
          className="overlay__close"
          onClick={() => setToggleMenu(false)}
          />
    
       <ul className="app__navbar-smallscreen-links">
      <li className="p__opensans">
        <a href="#home">на главную</a>
      </li>

       <li className="p__opensans">
         <a href="#about">о нас</a>
        </li>
        <li className="p__opensans">
          <a href="#menu">меню</a>
        </li> 
       
        <li className="p__opensans">
          <a href="#awards">награды</a>
        </li>
        <li className="p__opensans">
          <a href="#contacts">контакты</a>
        </li>
    </ul>
    </div>
    
    )}
  </nav>
  
)
  }
export default Navbar;
