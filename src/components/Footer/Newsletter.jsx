import React from 'react';

import { SubHeading } from '..';
import './Newsletter.css';

const Newsletter = () => (
  <div className="app__newsletter">
    <div className="app__newsletter-heading">
      <SubHeading title="Новостная рассылка" />
      <h1 className="headtext__cormorant">Lorem ipsum dolor sit amet</h1>
      <p className="p__opensans">Lorem ipsum, dolor sit amet consectetur adipisicing elit.</p>
    </div>

    <div className="app__newsletter-input flex__center">
      <input type="email" placeholder="Введите ваш email"/>
      <button className="custom__button">
        Подписаться
      </button>
    </div>
  </div>
);

export default Newsletter;

