import React from 'react';

import { SubHeading } from '../../components';
import { images, data } from '../../constants';
import './Laurels.css';


const AwardCard = ({ award: {imgUrl, title, subtitle} }) => {

  return (
  <div className="app__laurels-awards-card">
    <img 
      src={imgUrl} alt="изображение наград"
    />
   <div className="app__laurels-awards-card-content">
     <p className="p__cormorant" style={{color: "#DCCA87"}}>
       {title}
     </p>
     <p className="p__cormorant" style={{color: "#DCCA87"}}>
       {subtitle}
     </p>
  </div> 
  </div>
  )
}

const Laurels = () => (
  <section className="app__bg app__wrapper section__padding" id="awards" >
      <div className="app__wrapper-info">
          <SubHeading title="Награды и признание"/>
           <h1 className="headtext__cormorant">Наши почести</h1> 
      
            <div className="app__laurels-awards">
              {data.awards.map((award) => {
               return <AwardCard 
                  award={award}
                  key={award.title}
                />
              })}
            </div>
      </div>
      <div className="app__wrapper-img">
         <img src={images.laurels} alt="изображение блюда"/>       
      </div>
  </section>
);

export default Laurels;

