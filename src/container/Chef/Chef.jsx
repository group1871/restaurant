import React from 'react';

import { SubHeading } from '../../components'
import { images } from '../../constants'

import './Chef.css';

const Chef = () => (
  <div className="app__bg app__wrapper section__padding">
    <div className="app__wrapper-img app__wrapper-img-reverse">
      <img src={images.chef} alt="изображение шеф-повара" />
    </div>
    <div className="app__wrapper-info">
        <SubHeading title="Chefs Word"/>
        <h1 className="headtext__cormorant">Во Что Мы Верим</h1>
        <div className="app__chef-content">
          <div className="app__chef-content-quote">
            <img src={images.quote} alt="изображение кавычек" />
            <p className="p__opensans">Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
          </div>
          <p className="p__opensans">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, dolore earum iure ratione cupiditate quia numquam eligendi, sed sequi nisi perspiciatis deserunt itaque sit! Necessitatibus inventore voluptatibus eius suscipit magnam!
          </p>
        </div>

        <div className="app__chef-sign">
          <p>Kevin Luo</p>
          <p className="p__opensans">Шеф и Основатель</p>
          <img src={images.sign} alt="изображение подписи"/>
        </div>
    </div>
  </div>
);

export default Chef;
