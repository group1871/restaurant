import React from 'react';

import './Header.css';
import SubHeading from '../../components/SubHeading/SubHeading';
import {images} from '../../constants';

const Header = () => (
  <div className="app__header app__wrapper section__padding" id="home">
    <div className="app__wrapper-info">
      <SubHeading title="В погоне за новым вкусом"/>
    <h1 className="app__header-h1">Изысканная кухня</h1>
    <p className="p__opensans" style={{margin: '2rem 0'}}>
      Lorem ipsum, dolor sit amet consectetur adipisicing elit. Placeat harum odio rerum eum numquam doloribus sunt obcaecati, repellat ut iste, cupiditate expedita omnis est at consectetur sint eligendi corporis. Sint?
    </p>
    <button 
    type="button"
    className="custom__button">Посмотреть меню</button>
    </div>

    <div className="app__wrapper-img">
      <img src={images.welcome} alt="логотип приветствия"/>
    </div>
  </div>
);

export default Header;
