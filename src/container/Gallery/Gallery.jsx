import React, { useRef } from 'react';
import {BsInstagram, BsArrowLeftShort, BsArrowRightShort} from 'react-icons/bs';

import { SubHeading } from '../../components';
import { images } from '../../constants';

import './Gallery.css';

const galleryImages = [images.gallery01, images.gallery02, images.gallery03, images.gallery04];

const Gallery = () => {

  const scrollRef = useRef();

  const scroll = (direction) => {
    const { current } = scrollRef;

    if(direction === 'left') {
      current.scrollLeft -= 300;
    } else {
      current.scrollLeft += 300;
    }
  }

  return (
  <section className="app__gallery flex__center">
    <div className="app__gallery-content">
      <SubHeading title="Instagram"/>
      <h1 className="headtext__cormorant">Фотогалерея</h1>
      <p 
        className="p__opensans"
        style={{color: "#AAA", marginTop: '2rem'}}
      > Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique libero earum eum sint</p>
      <button 
        className="custom__button"
        type="button"  
        >Показать больше</button>
    </div>

    <div className="app__gallery-images">
      <div    className="app__gallery-images-container"
      ref={scrollRef}
      >
      {galleryImages.map((image, i) => (
        <div className="app__gallery-images-card flex__center" key={`gallery-image-${i + 1}`}>
          <img src={image} alt="галерея"/>
          <BsInstagram className="gallery__image-icon" />
        </div>
      ))}
      </div>
      <div className="app__gallery-images-arrow">
        <BsArrowLeftShort 
          className="gallery-arrow-icon"
          onClick={() => scroll('left')}
        />
          <BsArrowRightShort 
          className="gallery-arrow-icon"
          onClick={() => scroll('right')}
        />
      </div>

    </div>
  </section>
  );
}

export default Gallery;

