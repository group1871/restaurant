import React from 'react';

import {FiFacebook, FiTwitter, FiInstagram} from 'react-icons/fi'
import { FooterOverlay, Newsletter } from '../../components'
import { images } from '../../constants';

import './Footer.css';

const Footer = () => (
  <section className="app__footer section__padding">
    <FooterOverlay />
    <Newsletter />
    <div className="app__footer-links">
      <div className="app__footer-links-contact">
        <h1 className="app__footer-headtext">Наши контакты</h1>
        <p className="p__opensans">Lugana Streen SanFrancisco</p>
        <p className="p__opensans">89273027397</p>
        <p className="p__opensans">test@gmail.com</p>
      </div>
      <div className="app__footer-links-logo">
        <img src={images.gericht} alt="footer logo"/>
        <p className="p__opensans">Lorem, ipsum dolor sit amet consectetur adipisicing elit</p>
        <img src={images.spoon} className="spoon__img" style={{marginTop: 15}} alt="логотип ложки"/>
        <div className="app__footer-links-icons">
          <FiFacebook />
          <FiTwitter />
          <FiInstagram />
        </div>
      </div>
      <div className="app__footer-links-work">
      <h1 className="app__footer-headtext">Часы работы</h1>
        <p className="p__opensans">Понедельник-Пятница</p>
        <p className="p__opensans">09:00 - 19:00</p>
        <p className="p__opensans">Суббота-Воскресенье</p>
        <p className="p__opensans">09:00 - 22:00</p>
        
      </div>
    </div>
    <div className="footer__copyright">
      <p className="p__opensans">Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
    </div>
  </section>
);

export default Footer;

// 3:02